<style>
    td{
        border: 1px black solid;
        padding: 4px;
    }
</style>
<?php
$host = 'db'; //Nom donné dans le docker-compose.yml
$user = 'myuser'; // user et pwd du docker compose
$password = 'monpassword';
$db = 'tutoseu';
$conn = new mysqli($host,$user,$password,$db);

if(!$conn) {
    echo "Erreur de connexion à MSSQL<br />";
}
else
{
        // echo "Connexion à mySQL ok<br />";
        // mysqli_close($conn);

        $seances = "SELECT * FROM `Seances`";
        $query = $conn->query($seances);

        echo "<table>
                <tr>
                    <th>Id Séances</th>
                    <th>Activités</th>
                    <th>Dates</th>
                    <th>Note</th>
                </tr>";

        while($results = $query->fetch_assoc()){
            echo "<tr>";
            echo  "<td>" . $results['idSeances'] . "</td>";
            echo  "<td>" . $results['activitie'] . "</td>";
            echo  "<td>" . $results['date'] . "</td>";
            echo  "<td>" . $results['heure'] . "</td>";
            echo  "<td>" . $results['note'] . "</td>";
            echo "</tr>";

            // echo "<pre>";
            //var_dump($results);
            // echo ($results["name"]);
            // echo "</pre>";
        };
        
        // $db->close();

}

?>
