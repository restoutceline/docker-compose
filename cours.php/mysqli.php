<?php
//  requets de base avec mysqli en procedural :
include ('sql.inc');
 
// $bdd = mysqli_connect('serveur', 'utilisateur', 'mot_de_passe', 'base');
// permet aussi de pouvoir changer d'utilisateur
$bdd = mysqli_connect($host, $user, $pw, $db);
 
// ajouter une gestion d'erreur 
/* Vérification de la connexion */
if (mysqli_connect_errno()) {
 printf("Échec de la connexion : %s\n", mysqli_connect_error());
 exit();
}
 
// requete 
//$resultat = mysqli_query($bdd, 'requete');
$marequete = "SELECT * FROM `mytable` WHERE `id` == '12';" ;
$resultat = mysqli_query($bdd, $marequete ); // test table 1 
 
$req = ""; // string for req 2 
$resultat2 = mysqli_query($bdd, $req); // send req 2
// tester le format compact 
//$resultat = mysqli_query(mysqli_connect($host, $user, $pw, $db);, 'SELECT * FROM MyTable LIMIT 0, 40');
 
// boucle 1 - iteration simple 
 
echo "nombre de resultats : ";
echo $nbresults = mysqli_num_rows($resultat);
echo "<br/>";
 
echo "<br/>";
 
while($donnees = mysqli_fetch_assoc($resultat))
{
 echo $donnees['name'];
 echo $donnees['secu'];
 echo "<br/>"; // ou \n si on envoie en texte
// faire une mise en page differente
}
 
// boucle 2 avec autre base et printf
while ($row = mysqli_fetch_assoc($resultat2)) {
 echo "<pre>";
 printf ("%s \t %i \t (%s)\n", $row["email"], $myint ,$row["ip_address"]);
 echo "</pre>";
}
// on remarque que le curseur de la requete est à la fin 
// soit on renvoie la requete ...
//$resultat2 = mysqli_query($bdd, $req); // a decommenter pour activer 
// soit on remet l'index a zero
mysqli_data_seek($resultat2,0);
 
// boucle 3 
while ($row = mysqli_fetch_array($resultat2, MYSQLI_NUM) ) { // ou MYSQLI_ASSOC
echo "<pre style='color:red;'>";
printf ("%s (%s)\n", $row[2], $row[3]);
echo "</pre>";
}
 
mysqli_data_seek($resultat2,0);
 
// boucle 4 avec fetch_row
while ($row = mysqli_fetch_row($resultat2) ) { // ou MYSQLI_ASSOC
echo "<pre style='color:green;'>";
printf ("%s %s %s %s \n", $row[0] , $row[1], $row[2], $row[3]);
echo "</pre>";
}
 
/* Libération des résultats */
mysqli_free_result($resultat);
mysqli_free_result($resultat2);
/* Fermeture de la connexion */
 
//// les requetes preparées 
 
/*
ETAPES : 
- préparer la requête ;
- lier les variables à la requête ;
- exécuter la requête ;
- si la requête renvoie un résultat, on continue : lier le résultat à des variables ;
fetcher le résultat.dn't fetch mysqli in /var/www/html/php-tests/sql2/test1.php on line 79
*/
// initier un statement 
$stmt = mysqli_stmt_init($bdd);
// et variables 
$offset = 12 ;
$nbres = "mytable" ;
 
// on prepare la requete avec des ? comme placeholders
$req_pre = mysqli_stmt_prepare($stmt, "SELECT * FROM ? WHERE `id` == ? ;");
 
// on lie les params en fonction de leur presentation au serveur
mysqli_stmt_bind_param($stmt, "si", $nbres, $offset );
 
// execution de la requete
mysqli_stmt_execute($stmt);
 
// on lie les donnees aux resultats 
mysqli_stmt_bind_result($stmt, $donnees['id'], $donnees['name'], $donnees['secu'], $donnees['adr']);
 
// on boucle sur le tableau obtenu pour l'affichage
while(mysqli_stmt_fetch($stmt))
{
 echo $donnees['id'] . ", " . $donnees['secu'] . "<br/>";
}
//i un nombre entier
//d un nombre décimal
//s une chaîne de caractères
 
/* Fermeture du statement */
mysqli_stmt_close($stmt);
 
mysqli_close($bdd);
 
?>