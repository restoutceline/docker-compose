<?php

class Personnage {
    //private $nom = "manelle";
    protected $nom;
    public $genre;
    public $taille;
    public $intel;
    public $force;
    public $dexterite;
    public $apparence;
    public $vitesse;
    public $attaque;
    public $defense;

    public function __construct($n, $g){
        $this->nom = $n ;
        $this->genre = $g ; 
    }
    //getter
    public function get_nom() {
        return $this->nom ;
    }
    //setter
    public function set_nom($n){
        $this->nom = $n;
    }
}

class Magicien extends Personnage {
    public function get_nom(){
        return "*** " .$this->nom." ***" ;
    }
}

