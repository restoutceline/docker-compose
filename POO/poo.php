<?php

echo "vive la POO";

require 'classes/perso.class.php';

// class Personnage {
//     public $nom = "manelle";
//     public $type;
//     public $genre;
//     public $taille;
//     public $intel;
//     public $force;
//     public $dexterite;
//     public $apparence;
//     public $vitesse;
//     public $attaque;
//     public $defense;
// }

echo "<pre>";
$manelle = new Personnage("manuelita", "F");//paramètres constructeurs
$nico = new Personnage("nico","M");
// var_dump($manelle);
//echo $manelle->nom ; //get
//$manelle->nom = "manuela"; //set
// echo "<br>".$manelle->nom;
//echo $manelle->get_nom();//get
//$manelle->set_nom("manuela");//set
echo "<br>".$manelle->get_nom();//get

$manelle->malade = true ; //inférence
//echo "<br>".$manelle->malade;
var_dump($manelle);
var_dump($nico);

$oscar = new Magicien("Gandalfette", "F");
echo "<br>";
var_dump($oscar);

echo $oscar->get_nom();
